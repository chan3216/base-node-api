describe('config', () => {
  let config;

  describe('without env variables', () => {
    beforeEach(() => {
      config = require('./config');
    });

    test('should return default values', () => {
      expect(config).toEqual(expect.objectContaining({
        'app': expect.objectContaining({
          'port': expect.any(Number),
          'path': '/api',
        }),
        'db': expect.objectContaining({
          'name': expect.any(String),
          'username': expect.any(String),
          'password': expect.any(String),
          'options': expect.objectContaining({
            'dialect': 'mysql',
            'host': expect.any(String),
            'port': expect.any(Number),
            'database': expect.any(String),
            'logging': expect.any(Boolean),
          }),
        }),
      }));
    });
  });

  describe('with env variables', () => {
    let initialEnv;

    /* eslint-disable */
    beforeEach(() => {
      initialEnv = process.env;
      process.env.BASE_API_APP_PORT = '4000';
      process.env.BASE_API_APP_PATH = '/whatever';
      process.env.BASE_API_DB_NAME = 'db-name';
      process.env.BASE_API_DB_HOST = 'db-host';
      process.env.BASE_API_DB_PORT = '5000';
      process.env.BASE_API_DB_USERNAME = 'top';
      process.env.BASE_API_DB_PASSWORD = 'secret!';
      process.env.BASE_API_DB_ENABLE_LOGGING = 'true';
      jest.resetModules();
      config = require('./config');
    });
    /* eslint-enable */

    afterEach(() => {
      process.env = initialEnv;
      jest.resetModules();
    });

    test('should return env variables values', () => {
      expect(config).toEqual(expect.objectContaining({
        'app': expect.objectContaining({
          'port': 4000,
          'path': '/whatever',
        }),
        'db': expect.objectContaining({
          'name': 'db-name',
          'username': 'top',
          'password': 'secret!',
          'options': expect.objectContaining({
            'dialect': 'mysql',
            'host': 'db-host',
            'port': 5000,
            'database': 'db-name',
            'logging': true,
          }),
        }),
      }));
    });
  });
});
