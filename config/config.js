module.exports = {
  'app': {
    'port': parseInt(process.env.BASE_API_APP_PORT, 10) || 3000,
    'path': process.env.BASE_API_APP_PATH || '/api',
  },
  'db': {
    'name': process.env.BASE_API_DB_NAME || 'BASE_API',
    'username': process.env.BASE_API_DB_USERNAME || 'root',
    'password': process.env.BASE_API_DB_PASSWORD || 'password',
    'options': {
      'dialect': 'mysql',
      'host': process.env.BASE_API_DB_HOST || '127.0.0.1',
      'port': parseInt(process.env.BASE_API_DB_PORT, 10) || 3306,
      'database': process.env.BASE_API_DB_NAME || 'BASE_API',
      'logging': process.env.BASE_API_DB_ENABLE_LOGGING === 'true',
    },
  },
};
