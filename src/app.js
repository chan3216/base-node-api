const express = require('express');
const path = require('path');
const glob = require('glob');
const Sequelize = require('sequelize');
const config = require('../config/config');

class AppFactory {
  getApp() {
    const app = express();
    this._setupDatabase(app).then(() => {
      app.db.initialized = true;
    }).catch((e) => {
      // eslint-disable-next-line no-console
      console.error(e);
      process.exit(1);
    });
    this._setupMiddleware(app);
    this._setupRoutes(app);
    this._setupMiddlewarePost(app);
    app.info = {'startTime': new Date()};
    return app;
  }

  async _setupDatabase(app) {
    // fixes Sequelize warning. see https://github.com/sequelize/sequelize/issues/8417
    config.db.options.operatorsAliases = Sequelize.Op;
    config.db.options.define = {
      'charset': 'utf8mb4',
      'collate': 'utf8mb4_unicode_ci',
      'dialectOptions': {
        'collate': 'utf8mb4_unicode_ci',
        'supportBigNumbers': true,
      },
    };
    const sequelize = new Sequelize(config.db.name, config.db.username, config.db.password, config.db.options);
    sequelize.beforeDefine((attributes) => {
      attributes.updatedBy = {
        'type': 'VARCHAR(50)',
      };
    });

    app.db = this._setupModels(sequelize);
    app.Sequelize = Sequelize;
    app.sequelize = sequelize;
    // return is important here, to return the promise
    return this._updateDatabase(sequelize);
  }

  _setupModels(sequelize) {
    const modelFiles = glob.sync('src/**/model.js');
    const models = {};
    modelFiles.forEach((file) => {
      const model = sequelize.import(path.join(process.cwd(), file));
      models[model.name] = model;
    });
    Object.keys(models).forEach((modelName) => {
      if ('associate' in models[modelName]) {
        models[modelName].associate(models);
      }
    });
    return models;
  }

  _updateDatabase(sequelize) {
    // return is important here, to return the promise
    return sequelize.sync({'alter': true});
  }

  _setupRoutes(app) {
    const routesFiles = glob.sync('src/**/routes.js');
    routesFiles.forEach((file) => {
      require(path.join(process.cwd(), file))(app);
    });
  }

  _setupMiddleware(app) {
    const file = 'src/common/middleware.js';
    require(path.join(process.cwd(), file))(app);
  }

  _setupMiddlewarePost(app) {
    const file = 'src/common/middleware.post-routes.js';
    require(path.join(process.cwd(), file))(app);
  }
}

module.exports = new AppFactory().getApp();
