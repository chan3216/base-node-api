const app = require('./app');
const config = require('../config/config');

// missing unit test for next line
app.set('trust proxy', true);

app.listen(config.app.port, () => {
  // eslint-disable-next-line no-console
  console.log(`Labor-Boss API listening on ${config.app.port}`);
});
