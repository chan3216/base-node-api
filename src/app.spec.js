const Sequelize = require('sequelize');
const config = require('../config/config');

describe('app', () => {
  let app, sequelizeInstance;

  beforeAll(async() => {
    sequelizeInstance = new Sequelize();
    spyOn(sequelizeInstance, 'sync');
    app = require('./app');
  });

  const sampleRoutes = expect.arrayContaining([
    expect.objectContaining({
      'route': expect.objectContaining({
        'path': '/test-sample-routes',
        'methods': expect.objectContaining({
          'get': true,
          'post': true,
          'delete': true,
        }),
      }),
    }),
  ]);

  test('should add the api routes', () => {
    // eslint-disable-next-line no-underscore-dangle
    expect(app._router.stack).toEqual(sampleRoutes);
  });

  test('should setup sequelize with values from config', () => {
    expect(app.sequelize.constructorArguments).toEqual([
      config.db.name, config.db.username, config.db.password, config.db.options,
    ]);
  });

  describe('sync', () => {
    test('should sync database', () => {
      expect(app.sequelize.sync).toHaveBeenCalledTimes(1);
      expect(app.sequelize.sync).toHaveBeenCalledWith({'alter': true});
    });
  });

  describe('.db', () => {
    test('should export the models', () => {
      expect(app.db.SampleModel).toEqual(expect.any(Function));
      expect(app.db.SampleChildModel).toEqual(expect.any(Function));
      expect(app.db.SampleModel.name).toEqual('SampleModel');
      expect(app.db.SampleChildModel.name).toEqual('SampleChildModel');
    });

    test('models should be associated', () => {
      expect(app.db.SampleModel.associate).toHaveBeenCalledTimes(1);
      expect(app.db.SampleModel.associate).toHaveBeenCalledWith(app.db);
      expect(app.db.SampleChildModel.associate).toHaveBeenCalledTimes(1);
      expect(app.db.SampleChildModel.associate).toHaveBeenCalledWith(app.db);
    });
  });

  test('.sequelize should export sequelize instance', () => {
    expect(app.sequelize).toEqual(sequelizeInstance);
  });

  test('.Sequelize should export Sequelize class', () => {
    expect(app.Sequelize).toEqual(Sequelize);
  });

  test('.info.startTime should save an start date', () => {
    expect(app.info.startTime).toBeInstanceOf(Date);
  });
});
