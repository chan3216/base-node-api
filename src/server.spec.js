const config = require('../config/config');

describe('server', () => {
  let expressMock, expressInstanceMock, morganMock;

  beforeEach(() => {
    jest.resetModules();
    jest.setMock('express', require('../__mocks__/express'));
    jest.setMock('morgan', require('../__mocks__/morgan'));
    expressMock = require('express');
    morganMock = require('morgan');
    morganMock.mockClear();
    expressInstanceMock = expressMock();
    expressInstanceMock.listen = jest.fn();
    jest.spyOn(expressInstanceMock, 'use');
    require('./server');
  });

  afterEach(() => {
    jest.unmock('express');
    jest.unmock('morgan');
  });

  test('must listen on the configured port', () => {
    expect(expressInstanceMock.listen).toHaveBeenCalledTimes(1);
    expect(expressInstanceMock.listen.mock.calls[0][0]).toEqual(config.app.port);
  });

  describe('any environment', () => {
    test('should add morgan for logging', () => {
      expect(morganMock).toHaveBeenCalledTimes(1);
      expect(morganMock).toHaveBeenCalledWith('dev');
      expect(expressInstanceMock.use).toHaveBeenCalledWith(morganMock());
    });
  });

  describe('production environment', () => {
    let originalNodeEnv;

    beforeEach(() => {
      originalNodeEnv = process.env.NODE_ENV;
      process.env.NODE_ENV = 'production';
      jest.resetModules();
      jest.setMock('express', expressMock);
      morganMock.mockClear();
      require('./server');
    });

    afterEach(() => {
      process.env.NODE_ENV = originalNodeEnv;
      jest.unmock('express');
    });

    test('should add morgan for logging', () => {
      expect(morganMock).toHaveBeenCalledTimes(1);
      expect(morganMock).toHaveBeenCalledWith('combined');
      expect(expressInstanceMock.use).toHaveBeenCalledWith(morganMock());
    });
  });
});
