// see https://strongloop.com/strongblog/async-error-handling-expressjs-es7-promises-generators/#usinges7asyncawait
module.exports = (fn) => (...args) => fn(...args).catch(args[2]);
