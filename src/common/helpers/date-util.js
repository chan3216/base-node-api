const Moment = require('moment');
const {extendMoment} = require('moment-range');
const moment = extendMoment(Moment);
const timezoneOffset = require('get-timezone-offset');
const hoursToMillis = 60 * 60 * 1000;
const minutesToMillis = 60 * 1000;
const momentTimezone = require('moment-timezone');

module.exports = class {
  static getTimezoneOffset(timezone) {
    return timezoneOffset(timezone, new Date()) / 60;
  }

  static getDaysArray(startDateTimeInt, endDateTimeInt, timezone) {
    const range = moment.range(
      momentTimezone.tz(startDateTimeInt, timezone),
      momentTimezone.tz(endDateTimeInt, timezone));
    const days = Array.from(range.by('day', {'excludeEnd': false}));
    return days.map((m) => m.format('MM-DD-YYYY'));
  }

  static getStartAndEndTime(startDateTimeInt, endDateTimeInt, timezone) {
    const start = momentTimezone.tz(startDateTimeInt, timezone);
    const end = momentTimezone.tz(endDateTimeInt, timezone);
    const startOnlyTime = (start.hours() * hoursToMillis) + start.minutes() * minutesToMillis;
    const endOnlyTime = (end.hours() * hoursToMillis) + end.minutes() * minutesToMillis;

    return {startOnlyTime, endOnlyTime};
  }
};
