const DateUtil = require('./date-util');
const moment = require('moment-timezone');

describe('DateUtil', () => {
  describe('getDaysArray', () => {
    test('should return the correct values', () => {
      expect(DateUtil.getDaysArray(moment.tz('8/23/2018 23:55', 'MM/DD/YYYY HH:mm', 'America/Caracas').valueOf(),
        moment.tz('8/29/2018 12:00', 'MM/DD/YYYY HH:mm', 'America/Caracas').valueOf(), 'America/Caracas'))
        .toEqual(['08-23-2018', '08-24-2018', '08-25-2018', '08-26-2018', '08-27-2018', '08-28-2018']);
      expect(DateUtil.getDaysArray(moment.tz('8/23/2018 10:30', 'MM/DD/YYYY HH:mm', 'America/Caracas').valueOf(),
        moment.tz('8/29/2018 19:00', 'MM/DD/YYYY HH:mm', 'America/Caracas').valueOf(), 'America/Caracas'))
        .toEqual(['08-23-2018', '08-24-2018', '08-25-2018', '08-26-2018', '08-27-2018', '08-28-2018', '08-29-2018']);
      expect(DateUtil.getDaysArray(moment.tz('8/23/2018 23:00', 'MM/DD/YYYY HH:mm', 'America/Los_Angeles').valueOf(),
        moment.tz('8/29/2018 6:00', 'MM/DD/YYYY HH:mm', 'America/Los_Angeles').valueOf(), 'America/Los_Angeles'))
        .toEqual(['08-23-2018', '08-24-2018', '08-25-2018', '08-26-2018', '08-27-2018', '08-28-2018']);
      expect(DateUtil.getDaysArray(moment.tz('8/7/2018 22:00', 'MM/DD/YYYY HH:mm', 'America/Los_Angeles').valueOf(),
        moment.tz('8/10/2018 3:00', 'MM/DD/YYYY HH:mm', 'America/Los_Angeles').valueOf(), 'America/Los_Angeles'))
        .toEqual(['08-07-2018', '08-08-2018', '08-09-2018']);
    });
  });
  describe('getTimezoneOffset', () => {
    test('should return the correct values', () => {
      expect(DateUtil.getTimezoneOffset('America/Caracas')).toEqual(4);
      expect(DateUtil.getTimezoneOffset('America/Guayaquil')).toEqual(5);
      expect(DateUtil.getTimezoneOffset('Asia/Vladivostok')).toEqual(-10);
    });
  });
  describe('getStartAndEndTime', () => {
    test('should return the correct values', () => {
      expect(DateUtil.getStartAndEndTime(moment('8/23/2018 10:30 -04:00', 'MM/DD/YYYY HH:mm Z').valueOf(),
        moment('8/29/2018 19:00 -04:00', 'MM/DD/YYYY HH:mm Z').valueOf(), 'America/Caracas'))
        .toEqual({'endOnlyTime': 68400000, 'startOnlyTime': 37800000});
    });
  });
});
