module.exports = (sequelize, DataTypes) => {
  const Client = sequelize.define('Client', {
    'name': {'type': DataTypes.STRING, 'allowNull': false},
    'lastName': {'type': DataTypes.STRING, 'allowNull': false},
  });

  return Client;
};
