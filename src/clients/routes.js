module.exports = (app) => {
  const config = require('../../config/config');
  const appPath = config.app.path;
  const controller = require('./controller')(app);
  const routesWrapper = require('../common/routes-wrapper');

  app.route(`${appPath}/clients`)
    .get(routesWrapper(controller.findAll))
    .post(routesWrapper(controller.create));

  app.route(`${appPath}/clients/:id`)
    .get(routesWrapper(controller.findById))
    .put(routesWrapper(controller.update));
};
