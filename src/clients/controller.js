const Service = require('./service');

module.exports = (app) => {
  const service = new Service(app.db);

  return {

    async findAll(req, res) {
      const result = await service.findAll();
      res.json(result);
    },

    async create(req, res) {
      const result = await service.create(req.body);
      res.json(result);
    },

    async delete(req, res) {
      await service.delete(req.params.id);
      res.sendStatus(204);
    },

    async update(req, res) {
      const result = await service.update(req.params.id, req.body);
      res.json(result);
    },


    async findById(req, res) {
      const result = await service.findById(req.params.id);
      res.json(result);
    },

  };
};
