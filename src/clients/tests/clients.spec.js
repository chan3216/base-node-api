const supertest = require('supertest');
const app = require('../../app');
const config = require('../../../config/config');
const FixturesHelper = require('../../../test/fixtures/fixtures-helper');
const fixturesHelper = new FixturesHelper(app);

describe('clients', () => {
  const appPath = config.app.path;

  beforeAll(() => {
    return fixturesHelper.setup();
  });

  describe('GET /api/clients', () => {
    const executeValidGetRequest = () => {
      return supertest(app).get(`${appPath}/clients`);
    };

    test('should return clients', async() => {
      const response = await executeValidGetRequest().expect(200).expect('Content-Type', /json/);
      expect(response.body.length).toEqual(2);
      expect(response.body).toEqual(expect.arrayContaining([
        expect.objectContaining({
          'name': 'Jose',
          'lastName': 'Artigas',
        }),
        expect.objectContaining({
          'name': 'PEPE',
          'lastName': 'Mujica',
        }),
      ]));
    });
  });
});
