const ErrorHelper = require('../common/helpers/error-helper');

module.exports = class {
  constructor(models) {
    this.models = models;
    this.client = models.Client;
  }

  async findAll() {
    return await this.client.findAll();
  }

  async create(data) {
    return await this.client.create(data);
  }

  async update(id, data) {
    await this.client.update(data, {'where': {'id': id}});
    return await this.findById(id);
  }

  async delete(id) {
    await this.client.destroy({'where': {'id': id}});
  }

  async findById(id) {
    return await this.validateIfClientExistsAndReturn(id);
  }

  async validateIfClientExistsAndReturn(id) {
    const existingClient = await this.getClientWithAddress(id);
    if (existingClient === null) {
      throw ErrorHelper.buildError(404, 'No client');
    }
    return existingClient;
  }

  async getClientWithAddress(id) {
    return await this.client.findById(id);
  }
};
