const data = require('./data.json');

module.exports = class FixturesHelper {
  constructor(app) {
    this.app = app;
  }

  // eslint-disable-next-line max-statements
  async setup() {
    const db = this.app.db;

    if (db.fixturesInitialized) {
      return;
    }

    // recreate DB schema
    db.fixturesInitialized = true;
    await this.app.sequelize.sync({'force': true});

    const promises = [];

    data.client.forEach((client) => {
      promises.push(db.Client.create(client));
    });

    return Promise.all(promises);
  }

  async findOrCreateTestData() {
    // const db = this.app.db;
    // const templateData = data.templates.find((template) => {
    //   return template.id === id;
    // });
    // return db.Template.findOrCreate({
    //   'where': {id},
    //   'defaults': templateData,
    //   'include': [db.Template.Versions],
    // });
  }
};
