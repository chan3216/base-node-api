const Sequelize = require.requireActual('sequelize');
const dbConfig = require('./db-config');
const httpContext = require('express-http-context');

dbConfig.options.operatorsAliases = Sequelize.Op;
dbConfig.options.define = {
  'hooks': {
    'beforeCreate': (instance) => {
      const requestValue = httpContext.get('request');
      if (requestValue && requestValue.tokenInfo) {
        instance.updatedBy = requestValue.tokenInfo.uid;
      }
    },
  }};

const sequelizeInstance = new Sequelize(dbConfig.name, dbConfig.username, dbConfig.password, dbConfig.options);

const SequelizeMock = class {
  constructor() {
    // setting this variable just for testing that the constructor got called with the right arguments
    sequelizeInstance.constructorArguments = Array.from(arguments);
    sequelizeInstance.beforeDefine(function(attributes) {
      attributes.updatedBy = {
        'type': 'STRING',
      };
    });
    const originalSync = sequelizeInstance.sync;
    // Sequelize fails on tests with {alter: true} (because it uses sqlite).
    // That's why we need to mock it and call it without arguments
    sequelizeInstance.sync = jest.fn(() => originalSync.apply(sequelizeInstance));
    return sequelizeInstance;
  }
};

module.exports = SequelizeMock;
